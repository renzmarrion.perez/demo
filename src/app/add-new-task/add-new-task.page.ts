import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-add-new-task',
  templateUrl: './add-new-task.page.html',
  styleUrls: ['./add-new-task.page.scss'],
})
export class AddNewTaskPage implements OnInit {

  category =['Chores','Personal','Work','Shopping']
  taskName
  taskDate
  taskPriority
  taskCategory

  taskObject
  constructor(public modalCtrl: ModalController) { }
  async dismis(){ 
    await this.modalCtrl.dismiss(this.taskObject)

  }


  selectCategory(index){
    this.taskCategory = this.category[index]
  }
  addTask(){
    this.taskObject = ({
      itemName:this.taskName,
      itemDueDate: this.taskDate,
      itemPriority:this.taskPriority,
      itemCategory:this.taskCategory

      
      
    })
    this.dismis();
    console.log(this.taskObject)
    
    }
  ngOnInit() {
  }

}
